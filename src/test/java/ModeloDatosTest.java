import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.ModeloDatos;
public class ModeloDatosTest {
    @Test
    public void testExisteJugador() {
        System.out.println("Prueba de existeJugador");
        String nombre = "";
        ModeloDatos instance = new ModeloDatos();
        boolean expResult = false;
        boolean result = instance.existeJugador(nombre);
        assertEquals(expResult, result);
        //fail("Fallo forzado.");
    }

    @Test
    public void testActualizarJugador() {
        ModeloDatos modelo = mock(ModeloDatos.class);
        doNothing().when(modelo).actualizarJugador(isA(String.class));
        modelo.actualizarJugador("");
 
        verify(modelo, times(1)).actualizarJugador("");
    }
} 