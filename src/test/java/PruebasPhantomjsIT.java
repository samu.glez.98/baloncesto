import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
public class PruebasPhantomjsIT {
    private static WebDriver driver=null;

    @Test
    public void tituloIndexTest() {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,"/usr/bin/phantomjs");
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[] {"--web-security=no", "--ignore-ssl-errors=yes"});
        driver = new PhantomJSDriver(caps);
        driver.navigate().to("http://localhost:8080/Baloncesto/");
        assertEquals("Votacion mejor jugador liga ACB", driver.getTitle(), "El titulo no es correcto");
        System.out.println(driver.getTitle());
        driver.close();
        driver.quit();
    }

    @Test
    public void testVotosA0() {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,"/usr/bin/phantomjs");
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[] {"--web-security=no", "--ignore-ssl-errors=yes"});
        driver = new PhantomJSDriver(caps);
        driver.navigate().to("http://localhost:8080/Baloncesto/");
        driver.findElement(By.name("B3")).click();
        driver.findElement(By.name("B4")).click();
        assertEquals("0", driver.findElement(By.cssSelector("tr:nth-child(1) > td:nth-child(3)")).getText(), "Los votos no están a cero");
        assertEquals("0", driver.findElement(By.cssSelector("tr:nth-child(2) > td:nth-child(3)")).getText(), "Los votos no están a cero");
        assertEquals("0", driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(3)")).getText(), "Los votos no están a cero");
        assertEquals("0", driver.findElement(By.cssSelector("tr:nth-child(4) > td:nth-child(3)")).getText(), "Los votos no están a cero");
        driver.close();
        driver.quit();
    }

    @Test
    public void testVotoOtroJugador() {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,"/usr/bin/phantomjs");
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[] {"--web-security=no", "--ignore-ssl-errors=yes"});
        driver = new PhantomJSDriver(caps);
        driver.navigate().to("http://localhost:8080/Baloncesto/");
        driver.findElement(By.name("txtNombre")).click();
        driver.findElement(By.name("txtNombre")).sendKeys("Samuel");
        driver.findElement(By.cssSelector("p:nth-child(12) > input:nth-child(1)")).click();
        driver.findElement(By.name("txtOtros")).click();
        driver.findElement(By.name("txtOtros")).sendKeys("PruebaVotoOtroJugador");
        driver.findElement(By.name("B1")).click();
        driver.findElement(By.linkText("Ir al comienzo")).click();
        driver.findElement(By.name("B4")).click();
        assertEquals("1", driver.findElement(By.cssSelector("tr:nth-child(5) > td:nth-child(3)")).getText(), "Los votos no están a cero");
        driver.close();
        driver.quit();
    }
}
