package com;

public class Jugador{
    public int id;
    public String name;
    public int votes;

    public Jugador() {
    }

    public Jugador(int id, String name, int votes) {
        this.id = id;
        this.name = name;
        this.votes = votes;
    }

    public int getId() {
        return id; 
    }

    public String getName() {
        return name; 
    }

    public int getVotes() {
        return votes; 
    }

    public void setId(int id) {
        this.id = id; 
    }

    public void setName(String name) {
        this.name = name; 
    }

    public void setVotes(int votes) {
        this.votes = votes;
    }
}