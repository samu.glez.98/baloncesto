<!DOCTYPE html>
<%@ page import ="java.util.ArrayList,com.Jugador" %>
<html lang="es">
    <%
        ArrayList<Jugador> list = new ArrayList<Jugador>();
        list = (ArrayList<Jugador>) session.getAttribute("playerVotes");
    %>
<head>
    <link href="estilos.css" rel="stylesheet" type="text/css" />
    <title>Votos de los jugadores</title>
</head>

<body>
    <table>
        <thead>
            <tr>
                <th scope="col">Id</th>
                <th scope="col">Nombre</th>
                <th scope="col">Votos</th>
            </tr>
        </thead>
        <tbody>
            <% for(int i = 0; i < list.size(); i++) {
                Jugador jugador = new Jugador();
                jugador = list.get(i);
            %>
            <tr>
                <td><%=jugador.getId()%></td>
                <td><%=jugador.getName()%></td>
                <td><%=jugador.getVotes()%></td>
            </tr>
            <%
            };
            %>
        </tbody>
    </table>
    <br> <a href="index.html"> Ir al comienzo</a>
</body>
