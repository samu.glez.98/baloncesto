# Práctica Final ICDA - Samuel García González

Este es el repositorio en el que se ha desarrollado toda la práctica final de la asignatura ICDA del Máster de Desarrollo Ágil de Software para la Web en la UAH.

El objetivo de este trabajo era familiarizarse con la Integración Continua y aprender a manejar todas las herramientas de IC de GitLab.

Se han desarrollado nuevas características para una aplicación Java, siendo estas:
 - [Requisito 1](#req1)
 - [Prueba Unitaria](#pu)
 - [Requisito 2](#req2)
 - [Prueba Funcional A](#pfa)
 - [Prueba Funcional B](#pfb)
 - [Calidad de código](#qa)

Para el desarrollo se han creado 3 ramas diferentes para implementar las características. En la rama rama-req1-pu se ha desarrollado el [requisito 1](#req1) y la [prueba unitaria](#pu). En la rama rama-req2-pfa-pfb se ha desarrollado el [requisito 2](#req2) la [prueba funcional A](#pfa) y la [prueba funcional B](#pfb). Por último, en la rama [calidad de código](#qa) hemos mejorado el código de la apliccaión reduciendo el número de major issues, puesto que son necesarios menos de 20 major issues para que podamos desplegar la aplicación.

Como último comentario antes de abordar la implementación de las características, hay que destacar que la tarea de despliegue en Heroku en pre falla siempre en las ramas que no sean master, ya que Heroku las ignorará automáticamente, y no es un comportamiento que podamos modificar. En cuanto hacemos los merge request a main y se ejecuta esta tarea, podemos ver que siempre se completa correctamente. Los despliegues en heroku serán en los siguientes enlaces: <a href="https://baloncestosamu-pre.herokuapp.com/">Pre</a> y <a href="https://baloncestosamu.herokuapp.com/">Prod</a>, pero sólo funcionará el <a href="https://baloncestosamu.herokuapp.com/">Prod</a>, ya que Heroku sólo nos permite tener una aplicación accesible en el plan gratuito.

<a name="req1"></a>
## Requisito 1

Este requisito consiste en implementar un botón en la página para resetear los votos a 0. Este requisito fue complicado de implementar ya que fue la primera toma de contacto con el funcionamiento de la aplicación, pero una vez entendida, fue realmente sencillo de desarrollar. Desarrollamos este método en la clase ModeloDatos.java.

```java
    public void votosACero() {
        try {
            set = con.createStatement();
            set.executeUpdate("UPDATE Jugadores SET votos=0");
            rs.close();
            set.close();
        } catch (Exception e) {
            // No modifica la tabla
            System.out.println("No se han reseteado los votos");
            System.out.println("El error es: " + e.getMessage());
        }
    }
```
Además, refactorizamos el método service de la clase Acb.java para que sea más legible y se adecúe al nuevo comportamiento:

```java
    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        HttpSession s = req.getSession(true);
        String nombreP = (String) req.getParameter("txtNombre");
        String nombre = (String) req.getParameter("R1");
        if (nombreP != null && nombre != null) {
            if (nombre.equals("Otros")) {
                nombre = (String) req.getParameter("txtOtros");
            }
            if (bd.existeJugador(nombre)) {
                bd.actualizarJugador(nombre);
            } else {
                bd.insertarJugador(nombre);
            }
            s.setAttribute("nombreCliente", nombreP);
            // Llamada a la página jsp que nos da las gracias
            res.sendRedirect(res.encodeRedirectURL("TablaVotos.jsp"));
        } else if (req.getParameter("B3") != null){
            bd.votosACero();
            res.sendRedirect(res.encodeRedirectURL("index.html"));
        }
    }
```

Por último, añadimos un botón para poner los votos a cero a la clase index.html:

```html
            <input type="submit" name="B3" value="Votos a cero"></p>
```

<a name="pu"></a>
## Prueba Unitaria

En la prueba unitaria, se ha seguido las recomendaciones encontradas en <a href="https://www.baeldung.com/mockito-void-methods">Baeldung</a> sobre cómo testear métodos void. Es un test bastante sencillo que comprueba que se llama al método correctamente y sólo una vez. Aquí tenemos la implementación:

```java
    @Test
    public void testActualizarJugador() {
        ModeloDatos modelo = mock(ModeloDatos.class);
        doNothing().when(modelo).actualizarJugador(isA(String.class));
        modelo.actualizarJugador("");
 
        verify(modelo, times(1)).actualizarJugador("");
    }
```

<a name="req2"></a>
## Requisito 2

En este requisito debemos implementar un botón nuevo para poder ver los votos de los jugadores en una página nueva. Este requisito no fue muy complicado de desarrollar, hubo bastantes problemas con los imports de clases en la nueva página VerVotos.jsp, por lo que hubo que realojar el código Java de la aplicación en un paquete llamado com, ya que al no haber un paquete, no reconocía correctamente el import necesario para usar el modelo Jugador.java.

Por lo demás, es bastante sencillo de entender. Antes de nada, debemos crear un modelo Jugador, para poder usarlo en un ArrayList:


```java
package com;

public class Jugador{
    public int id;
    public String name;
    public int votes;

    public Jugador() {
    }

    public Jugador(int id, String name, int votes) {
        this.id = id;
        this.name = name;
        this.votes = votes;
    }

    public int getId() {
        return id; 
    }

    public String getName() {
        return name; 
    }

    public int getVotes() {
        return votes; 
    }

    public void setId(int id) {
        this.id = id; 
    }

    public void setName(String name) {
        this.name = name; 
    }

    public void setVotes(int votes) {
        this.votes = votes;
    }
}
```

En segundo lugar implementamos la consulta a la base de datos:

```java
    public ArrayList<Jugador> recuperarJugadores() {
        ArrayList<Jugador> list = new ArrayList<Jugador>();
        try {
            set = con.createStatement();
            rs = set.executeQuery("SELECT * FROM Jugadores");
            while (rs.next()) {
                Jugador jugador = new Jugador();
                jugador.setId(rs.getInt("id"));
                jugador.setName(rs.getString("nombre"));
                jugador.setVotes(rs.getInt("votos"));
                list.add(jugador);
            }
            rs.close();
            set.close();
        } catch (Exception e) {
            // No inserta en la tabla
            System.out.println("No inserta en la tabla");
            System.out.println("El error es: " + e.getMessage());
        }
        return list;
    }
```

En tercer lugar, implementamos el comportamiento del nuevo botón en el servicio de la clase Acb.java (en la rama rama-req2-pfa-pfb existe un else if más, un botón llamado B5, que ha sido corregido directamente en la rama master, ya que es un resto del desarrollo que no debería estar):

```java
    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        HttpSession s = req.getSession(true);
        String nombreP = (String) req.getParameter("txtNombre");
        String nombre = (String) req.getParameter("R1");
        ArrayList<Jugador> list = new ArrayList<Jugador>();
        if (nombreP != null && nombre != null) {
            if (nombre.equals("Otros")) {
                nombre = (String) req.getParameter("txtOtros");
            }
            if (bd.existeJugador(nombre)) {
                bd.actualizarJugador(nombre);
            } else {
                bd.insertarJugador(nombre);
            }
            s.setAttribute("nombreCliente", nombreP);
            // Llamada a la página jsp que nos da las gracias
            res.sendRedirect(res.encodeRedirectURL("TablaVotos.jsp"));
        } else if (req.getParameter("B3") != null){
            bd.votosACero();
            res.sendRedirect(res.encodeRedirectURL("index.html"));
        } else if (req.getParameter("B4") != null){
            list = bd.recuperarJugadores();
            s.setAttribute("playerVotes", list);
            res.sendRedirect(res.encodeRedirectURL("VerVotos.jsp"));
        }
    }
```

En cuarto lugar, creamos el botón para ver la nueva página:

```html
            <input type="submit" name="B4" value="Ver votos"></p>
```

Por último, la nueva página en la que se mostrarán la tabla de los jugadores:

```html
<%@ page import ="java.util.ArrayList,com.Jugador" %>

<html>
    <%
        ArrayList<Jugador> list = new ArrayList<Jugador>();
        list = (ArrayList<Jugador>) session.getAttribute("playerVotes");
    %>
<head>
    <link href="estilos.css" rel="stylesheet" type="text/css" />
    <title>Votos de los jugadores</title>
</head>

<body>
    <table>
        <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Votos</th>
            </tr>
        </thead>
        <tbody>
            <% for(int i = 0; i < list.size(); i++) {
                Jugador jugador = new Jugador();
                jugador = list.get(i);
            %>
            <tr>
                <td><%=jugador.getId()%></td>
                <td><%=jugador.getName()%></td>
                <td><%=jugador.getVotes()%></td>
            </tr>
            <%
            };
            %>
        </tbody>
    </table>
    <br> <a href="index.html"> Ir al comienzo</a>
</body>
```

<a name="pfa"></a>
## Prueba Funcional A

Esta prueba es para comprobar que el botón de poner votos a 0 funciona correctamente. Implementarla fue bastante sencillo, ya que Selenium es bastante fácil de usar y nos ofrece una capacidad de testing muy potente.

El código una vez exportado a java y modificado para funcionar con PhantomJS queda así:

```java
    @Test
    public void testVotosA0() {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,"/usr/bin/phantomjs");
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[] {"--web-security=no", "--ignore-ssl-errors=yes"});
        driver = new PhantomJSDriver(caps);
        driver.navigate().to("http://localhost:8080/Baloncesto/");
        driver.findElement(By.name("B3")).click();
        driver.findElement(By.name("B4")).click();
        assertEquals("0", driver.findElement(By.cssSelector("tr:nth-child(1) > td:nth-child(3)")).getText(), "Los votos no están a cero");
        assertEquals("0", driver.findElement(By.cssSelector("tr:nth-child(2) > td:nth-child(3)")).getText(), "Los votos no están a cero");
        assertEquals("0", driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(3)")).getText(), "Los votos no están a cero");
        assertEquals("0", driver.findElement(By.cssSelector("tr:nth-child(4) > td:nth-child(3)")).getText(), "Los votos no están a cero");
        driver.close();
        driver.quit();
    }
```

<a name="pfb"></a>
## Prueba Funcional B

Esta prueba comprueba que al votar a otro jugador se registra correctamente su voto. En la línea de la anterior prueba, es bastante similar, ya que usamos igual Selenium y el driver de PhantomJS. Este es el código una vez adaptado:

```java
    @Test
    public void testVotoOtroJugador() {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,"/usr/bin/phantomjs");
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[] {"--web-security=no", "--ignore-ssl-errors=yes"});
        driver = new PhantomJSDriver(caps);
        driver.navigate().to("http://localhost:8080/Baloncesto/");
        driver.findElement(By.name("txtNombre")).click();
        driver.findElement(By.name("txtNombre")).sendKeys("Samuel");
        driver.findElement(By.cssSelector("p:nth-child(12) > input:nth-child(1)")).click();
        driver.findElement(By.name("txtOtros")).click();
        driver.findElement(By.name("txtOtros")).sendKeys("PruebaVotoOtroJugador");
        driver.findElement(By.name("B1")).click();
        driver.findElement(By.linkText("Ir al comienzo")).click();
        driver.findElement(By.name("B4")).click();
        assertEquals("1", driver.findElement(By.cssSelector("tr:nth-child(5) > td:nth-child(3)")).getText(), "Los votos no están a cero");
        driver.close();
        driver.quit();
    }
```
<a name="qa"></a>
## Calidad de código

Esta característica la implementamos creando una nueva regla en sonarqube para que si detecta más de 20 major issues, falle esa fase en el pipeline, pero aún así permita desplegar la aplicación, ya que la calidad no impide que la aplicación funcione. Las mejoras de calidad para reducir el número de major issues fueron varias, como cambiar elementos deprecados de html, añadir DOCTYPE y el lenguaje a los html y añadir @Override a los métodos que extiende la clase Acb de HttpServlet. Con esas mejoras, que podemos consultar en los commits de la rama rama-qa, reducimos el número de major issues a 15 y nos permite pasar la fase de qa satisfactoriamente.